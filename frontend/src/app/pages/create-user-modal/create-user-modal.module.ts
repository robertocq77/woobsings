import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateUserModalPageRoutingModule } from './create-user-modal-routing.module';

import { CreateUserModalPage } from './create-user-modal.page';
import { MaterialModulesModule } from '../../modules/material-modules/material-modules.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateUserModalPageRoutingModule,
    MaterialModulesModule
  ],
  declarations: [CreateUserModalPage]
})
export class CreateUserModalPageModule {}
