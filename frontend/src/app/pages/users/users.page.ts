import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../providers/users.service';
import { ModalController } from '@ionic/angular';
import { CreateUserModalPage } from '../create-user-modal/create-user-modal.page';

@Component({
  selector: 'app-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss'],
})
export class UsersPage implements OnInit {
  public users: Array<any> = [];

  constructor(
    public usersService: UsersService,
    public modalController: ModalController,
  ) { }

  ngOnInit() {
    this.usersService.getAllUsers({limit: 10}).then((res: any) => {
      this.users = res.data;
      console.log(res);
    });
    this.openModal();
  }
  public async openModal() {
    const modal = await this.modalController.create({
      component: CreateUserModalPage,
      cssClass: 'my-custom-class',
    });
    return await modal.present();
  }
}
