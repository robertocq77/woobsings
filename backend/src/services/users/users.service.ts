import { Injectable } from '@nestjs/common';

import * as firebase from "firebase/app";

// Add the Firebase services that you want to use
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyDL-THlXREO4vZ3chq9i6VXqyTKxkcxy2Q",
  authDomain: "prueba-3b0a1.firebaseapp.com",
  databaseURL: "https://prueba-3b0a1.firebaseio.com",
  projectId: "prueba-3b0a1",
  storageBucket: "prueba-3b0a1.appspot.com",
  messagingSenderId: "589914306414",
  appId: "1:589914306414:web:65ffb953bff003ec29337f"
};

firebase.initializeApp(firebaseConfig);


@Injectable()
export class UsersService {
  public db = firebase.firestore();


    public async createUser(user: any){
		  return await this.db.collection('users').add(user);
    }

    public getAllUsers(params: any){
      return new Promise((resolve, reject) => {
        let response = [];
        this.db.collection('users').get()
        .then((querySnapshots) => {
          querySnapshots.forEach((doc) => {
            response.push(doc.data())
          })
          resolve(response)
        }).catch((error) => {
          reject(error)
        })
      })

    }

    public getUser(id: string): any{
		return '';
    }

    public updateUser(id: string, data: object): string{
		return 'User has been updated';
    }
    
    public deleteUser(id: string) : string{
		return 'User has been deleted'
    }
}
